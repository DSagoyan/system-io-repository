﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace System_IO_Repository.Repository
{
    abstract class BaseRepository<T> where T : class, new()
    {
        public abstract string path { get; }

        public int Save(IEnumerable<T> models)
        {
            int count = 0;
            StreamWriter writer = new StreamWriter(path, true, Encoding.Default);
            foreach (var item in models)
            {
                writer.WriteLine("{");
                PrintModels(writer, item);

                writer.WriteLine("}");
                count++;
            }
            writer.Dispose();
            return count;
        }

        public abstract void PrintModels(StreamWriter writer, T model);
    }
}
