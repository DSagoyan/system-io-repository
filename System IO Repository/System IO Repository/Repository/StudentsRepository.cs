﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System_IO_Repository.Models;

namespace System_IO_Repository.Repository
{
    class StudentsRepository: BaseRepository<Students>
    {
        public override string path => "Text.txt";

        public override void PrintModels(StreamWriter writer, Students model)
        {
            writer.WriteLine($"{nameof(model.Name)} : {model.Name}");
            writer.WriteLine($"{nameof(model.Surname)} : {model.Surname}");
            writer.WriteLine($"{nameof(model.University)}: {model.University}");
            writer.WriteLine($"{ nameof(model.age)} : { model.age}");
        }
    }
}
