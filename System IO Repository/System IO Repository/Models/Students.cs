﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System_IO_Repository;


namespace System_IO_Repository.Models
{
    class Students
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public University University { get; set; }
        public byte age { get; set; }

        public string FullName => $"{Name} {Surname}";

        public override string ToString() => FullName;

    }
}
