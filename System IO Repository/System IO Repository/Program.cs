﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System_IO_Repository.Models;
using System_IO_Repository.Repository;

namespace System_IO_Repository
{
    class Program
    {
        static void Main(string[] args)
        {
            var student = CreateStudents(10);
            var temp = new StudentsRepository();

            temp.Save(student);
        }

        static IEnumerable<Students> CreateStudents (int num)
        {
            Random rnd = new Random();

            string[] names =
            {
                "Aram",
                "Suren",
                "Davit",
                "Arman",
                "Karen"

            };

            string[] surnames =
            {
                "Davtyan",
                "Mkrtchyan",
                "Grigoryan",
                "Savzyan",
                "Mkrtymyan"
            };

            for (int i = 0; i < num; i++)
            {
                var index = rnd.Next(0, names.Length);
                var index1 = rnd.Next(0, surnames.Length);

                yield return new Students
                {
                    Name = names[index],
                    Surname = surnames[index1],
                    age = (byte)rnd.Next(25, 50),
                    University = (University) rnd.Next(0, 3)
                };
            }
        }
    }
}
